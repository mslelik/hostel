﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hostel.Data;
using System.Security;
using System.Security.Cryptography;

namespace Hostel.Client
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        public Employee User { get; set; }

        private void DoLogin()
        {
            string login = textBox_Login.Text;
            string passwordHash = GetHash(textBox_Password.Password);

            using (HostelDBEntities db = new HostelDBEntities())
            {
                User = db.Employees.Where(x => x.Login == login).FirstOrDefault();
                if (User != null)
                {
                    if (User.PasswordHash.Trim() == passwordHash)
                        DialogResult = true;
                    else DialogResult = false;
                }
                else DialogResult = false;
            }
        }

        private string GetHash(string password)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (var b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }

        private void button_Login_Click(object sender, RoutedEventArgs e)
        {
            DoLogin();
        }

        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
