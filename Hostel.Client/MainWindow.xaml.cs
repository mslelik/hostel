﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hostel.Client.ViewModels;
using Hostel.Data;

namespace Hostel.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private StudentsViewModel studentsViewModel = new StudentsViewModel();
        private ContractsViewModel contractsViewModel = new ContractsViewModel();
        private RoomsViewModel roomsViewModel = new RoomsViewModel();
        private JournalViewModel journalViewModel = new JournalViewModel();
        private LinenViewModel linenViewModel = new LinenViewModel();
        private LinenJournalsViewModel linenJournalsViewModel = new LinenJournalsViewModel();
        private FurnitureViewModel furnitureViewModel = new FurnitureViewModel();

        private Employee user;

        public MainWindow()
        {
            InitializeComponent();

            while (!DisplayLoginWindow()) ;

            if (user.Position == "комендант")
            {
                tabItem_Linen.Visibility = System.Windows.Visibility.Collapsed;
            }
            else if (user.Position == "кастелянша")
            {
                tabItem_Linen.IsSelected = true;
                tabItem_Contracts.Visibility = System.Windows.Visibility.Collapsed;
                tabItem_Furniture.Visibility = System.Windows.Visibility.Collapsed;
                tabItem_Journal.Visibility = System.Windows.Visibility.Collapsed;
            }

            students.DataContext = studentsViewModel;
            contracts.DataContext = contractsViewModel;
            rooms.DataContext = roomsViewModel;
            journal.DataContext = journalViewModel;
            linen.DataContext = linenViewModel;
            linenJournal.DataContext = linenJournalsViewModel;
            roomsFurniture.DataContext = roomsViewModel;
            furniture.DataContext = furnitureViewModel;
        }

        protected override void OnClosed(EventArgs e)
        {
            studentsViewModel.Dispose();
            contractsViewModel.Dispose();
            roomsViewModel.Dispose();
            journalViewModel.Dispose();
            linenViewModel.Dispose();
            linenJournalsViewModel.Dispose();
            furnitureViewModel.Dispose();

            base.OnClosed(e);
        }

        private void studentsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Student student = ((DataGrid)sender).SelectedItem as Student;
            if (student != null)
            {
                roomsViewModel.GetRoomByStudent(student);
                contractsViewModel.GetContractByStudent(student);
                journalViewModel.GetJournalRecordsByStudent(student);
                linenJournalsViewModel.GetLinenJournalRecordsByStudent(student);
            }
        }

        private void roomsGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            roomsViewModel.IsAdding = true;
        }

        private void studentsGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            studentsViewModel.IsAdding = true;
        }

        private void contractsGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            contractsViewModel.IsAdding = true;
            contractsViewModel.Student = (studentsGrid.SelectedItem) as Student;
        }

        private void linenGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            linenViewModel.IsAdding = true;
        }

        private void journalGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            journalViewModel.IsAdding = true;
            journalViewModel.Student = (studentsGrid.SelectedItem) as Student;
        }

        private void linenJournalGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            linenJournalsViewModel.IsAdding = true;
            linenJournalsViewModel.Student = (studentsGrid.SelectedItem) as Student;
            linenJournalsViewModel.Linen = (linenGrid.SelectedItem) as Linen;
        }

        private void linenJournalGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LinenJournal record = ((DataGrid)sender).SelectedItem as LinenJournal;
            if (record != null && record.LinenJournalId != 0)
            {
                linenViewModel.GetLinenByJournalRecord(record);
            }
        }

        private void roomsFurnitureGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            roomsViewModel.IsAdding = true;
        }

        private void roomsFurnitureGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Room room = ((DataGrid)sender).SelectedValue as Room;
            if (room != null && room.RoomId != 0)
            {
                furnitureViewModel.Room = room;
                furnitureViewModel.GetFurnitureByRoom(room);
            }
        }

        private void furnitureGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            furnitureViewModel.IsAdding = true;
        }

        private bool DisplayLoginWindow()
        {
            LoginWindow lw = new LoginWindow();
            lw.ShowDialog();
            if (lw.DialogResult.HasValue && lw.DialogResult.Value)
            {
                MessageBox.Show("Вход выполнен успешно", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Information);
                user = lw.User;
                return true;
            }
            else
            {
                MessageBox.Show("Вход не удалось выполнить", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.Close();
                return false;
            }
        }
    }
}
