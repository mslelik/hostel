﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;

namespace Hostel.Client.ViewModels
{
    public class LinenJournalsViewModel : ViewModelBase
    {
        public LinenJournalsViewModel()
        {
            dataContext = new HostelDBEntities();
            LinenJournalRecords = new ObservableCollection<LinenJournal>(dataContext.LinenJournals);

            Add = new ActionCommand(AddLinenJournalRecord) { IsExecutable = true };
            Remove = new ActionCommand(RemoveLinenJournalRecord) { IsExecutable = true };
            ViewAllRecords = new ActionCommand(AllRecords) { IsExecutable = true };
        }

        public ObservableCollection<LinenJournal> LinenJournalRecords { get; set; }

        private LinenJournal selectedItem;
        public LinenJournal SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public Student Student { get; set; }

        public Linen Linen { get; set; }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        public ActionCommand ViewAllRecords { get; set; }

        private void AddLinenJournalRecord()
        {
            if (SelectedItem != null && Student != null && Linen != null)
            {
                SelectedItem.StudentId = Student.StudentId;
                SelectedItem.LinenId = Linen.LinenId;
                dataContext.LinenJournals.Add(SelectedItem);
                base.SaveChanges();

                AllRecords();

                IsAdding = false;
            }
        }

        private void RemoveLinenJournalRecord()
        {
            if (SelectedItem != null)
            {
                dataContext.LinenJournals.Find(SelectedItem.LinenJournalId).Student = null;
                dataContext.LinenJournals.Find(SelectedItem.LinenJournalId).Linen = null;
                LinenJournal j = dataContext.LinenJournals.Find(SelectedItem.LinenJournalId);
                dataContext.LinenJournals.Remove(j);
                base.SaveChanges();

                AllRecords();
            }
        }

        private void AllRecords()
        {
            LinenJournalRecords.Clear();

            foreach (var record in dataContext.LinenJournals)
            {
                LinenJournalRecords.Add(record);
            }
        }

        public void GetLinenJournalRecordsByStudent(Student student)
        {
            LinenJournalRecords.Clear();
            foreach (var record in student.LinenJournals)
            {
                LinenJournalRecords.Add(record);
            }
        }
    }
}
