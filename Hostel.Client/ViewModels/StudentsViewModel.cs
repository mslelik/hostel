﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;

namespace Hostel.Client.ViewModels
{
    public class StudentsViewModel : ViewModelBase, IDisposable
    {
        public StudentsViewModel()
        {
            dataContext = new HostelDBEntities();
            Students = new ObservableCollection<Student>(dataContext.Students);
            Institutes = new ObservableCollection<string>() { "ИМИКТ", "ИСИА", "ЛТИ", "ИЭИТ", "ИТИПХ", "ИЭИУ", "ИФИМК", "ИНИГ" };

            Search = new ActionCommand(SearchByLastName) { IsExecutable = true };
            Add = new ActionCommand(AddStudent) { IsExecutable = true };
            Remove = new ActionCommand(RemoveRoom) { IsExecutable = true };
            Update = new ActionCommand(UpdateStudents) { IsExecutable = true };
        }

        public ObservableCollection<Student> Students { get; private set; }

        public ObservableCollection<string> Institutes { get; private set; }

        private Student selectedItem;
        public Student SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value || value.LastName == null) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        public ActionCommand Update { get; set; }

        public ActionCommand Search { get; set; }

        public string SearchCriteria { get; set; }

        private void AddStudent()
        {
            if (SelectedItem != null)
            {
                dataContext.Students.Add(SelectedItem);
                IsAdding = false;

                Students.Clear();
                foreach (var student in dataContext.Students)
                {
                    Students.Add(student);
                }
            }
        }

        private void RemoveRoom()
        {
            if (SelectedItem != null)
            {
                dataContext.Students.Remove(SelectedItem);
                base.SaveChanges();

                AllStudents();
            }
        }

        private void AllStudents()
        {
            Students.Clear();
            foreach (var students in dataContext.Students)
            {
                Students.Add(students);
            }
        }

        private void UpdateStudents()
        {
            dataContext = new HostelDBEntities();
            AllStudents();
        }

        private void SearchByLastName()
        {
            Students.Clear();

            foreach (var student in dataContext.Students.ToList().Where(x => x.LastName.Contains(SearchCriteria)))
            {
                Students.Add(student);
            }
        }
    }
}
