﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;

namespace Hostel.Client.ViewModels
{
    public class JournalViewModel : ViewModelBase
    {
        public JournalViewModel()
        {
            dataContext = new HostelDBEntities();
            JournalRecords = new ObservableCollection<Journal>(dataContext.Journals);

            Add = new ActionCommand(AddJournalRecord) { IsExecutable = true };
            Remove = new ActionCommand(RemoveJournalRecord) { IsExecutable = true };
            ViewAllRecords = new ActionCommand(AllRecords) { IsExecutable = true };
        }
        public ObservableCollection<Journal> JournalRecords { get; set; }

        private Journal selectedItem;
        public Journal SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public Student Student { get; set; }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        public ActionCommand ViewAllRecords { get; set; }

        private void AddJournalRecord()
        {
            if (SelectedItem != null && Student != null)
            {
                SelectedItem.StudentId = Student.StudentId;
                dataContext.Journals.Add(SelectedItem);
                base.SaveChanges();

                AllRecords();

                IsAdding = false;
            }
        }

        private void RemoveJournalRecord()
        {
            if (SelectedItem != null)
            {
                dataContext.Journals.Find(SelectedItem.JournalId).Student = null;
                Journal j = dataContext.Journals.Find(SelectedItem.JournalId);
                dataContext.Journals.Remove(j);
                base.SaveChanges();

                AllRecords();
            }
        }

        private void AllRecords()
        {
            JournalRecords.Clear();
            foreach (var record in dataContext.Journals)
            {
                JournalRecords.Add(record);
            }
        }

        public void GetJournalRecordsByStudent(Student student)
        {
            JournalRecords.Clear();
            foreach (var record in student.Journals)
            {
                JournalRecords.Add(record);
            }
        }
    }
}
