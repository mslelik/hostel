﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;

namespace Hostel.Client.ViewModels
{
    public class ContractsViewModel : ViewModelBase
    {
        public ContractsViewModel()
        {
            dataContext = new HostelDBEntities();
            Contracts = new ObservableCollection<Contract>(dataContext.Contracts);

            Add = new ActionCommand(AddContract) { IsExecutable = true };
            Remove = new ActionCommand(RemoveContract) { IsExecutable = true };
            ViewAllContracts = new ActionCommand(AllContracts) { IsExecutable = true };
        }

        public ObservableCollection<Contract> Contracts { get; private set; }

        private Contract selectedItem;
        public Contract SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        private Student student;
        public Student Student { get; set; }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        public ActionCommand ViewAllContracts { get; set; }

        private void AddContract()
        {
            if (SelectedItem != null && Student != null)
            {
                dataContext.Contracts.Add(SelectedItem);
                base.SaveChanges();

                dataContext.Students.Where(x => x.StudentId == Student.StudentId).First().ContractId = SelectedItem.ContractId;
                base.SaveChanges();

                IsAdding = false;
            }
        }

        private void RemoveContract()
        {
            if (SelectedItem != null)
            {
                dataContext.Contracts.Find(SelectedItem.ContractId).Students.Clear();
                Contract c = dataContext.Contracts.Find(SelectedItem.ContractId);
                dataContext.Contracts.Remove(c);
                base.SaveChanges();

                AllContracts();
            }
        }

        private void AllContracts()
        {
            Contracts.Clear();
            foreach (var contract in dataContext.Contracts)
            {
                Contracts.Add(contract);
            }
        }

        public void GetContractByStudent(Student student)
        {
            Contracts.Clear();
            if (student.ContractId != null)
                Contracts.Add(student.Contract);
        }
    }
}
