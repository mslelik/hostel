﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;

namespace Hostel.Client.ViewModels
{
    public class LinenViewModel : ViewModelBase
    {
        public LinenViewModel()
        {
            dataContext = new HostelDBEntities();
            Linens = new ObservableCollection<Linen>();

            Add = new ActionCommand(AddLinen) { IsExecutable = true };
            Remove = new ActionCommand(RemoveLinen) { IsExecutable = true };
        }

        public ObservableCollection<Linen> Linens { get; set; }

        private Linen selectedItem;
        public Linen SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        private void AddLinen()
        {
            if (SelectedItem != null)
            {
                dataContext.Linens.Add(SelectedItem);
                IsAdding = false;
            }
        }

        private void RemoveLinen()
        {
            if (SelectedItem != null)
            {
                dataContext.Linens.Remove(SelectedItem);
                base.SaveChanges();
                Linens.Clear();

                foreach (var linen in dataContext.Linens)
                {
                    Linens.Add(linen);
                }
            }
        }

        public void GetLinenByJournalRecord(LinenJournal record)
        {
            Linens.Clear();

            Linens.Add(record.Linen);
        }
    }
}
