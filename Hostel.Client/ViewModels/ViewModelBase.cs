﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;

namespace Hostel.Client.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        public ViewModelBase()
        {
            Save = new ActionCommand(SaveChanges) { IsExecutable = true };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public HostelDBEntities dataContext;

        public ActionCommand Save { get; set; }

        private bool isAdding;
        public bool IsAdding
        {
            get { return isAdding; }
            set
            {
                if (isAdding == value) return;
                isAdding = value;
                RaisePropertyChanged("IsAdding");
            }
        }

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        public void RaisePropertyChanged(string property)
        {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void SaveChanges()
        {
            dataContext.SaveChanges();
        }

        public void Dispose()
        {
            dataContext.Dispose();
        }
    }
}
