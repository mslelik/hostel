﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;

namespace Hostel.Client.ViewModels
{
    public class FurnitureViewModel : ViewModelBase
    {
        public FurnitureViewModel()
        {
            dataContext = new HostelDBEntities();
            Furnitures = new ObservableCollection<Furniture>();

            Add = new ActionCommand(AddFurniture) { IsExecutable = true };
            Remove = new ActionCommand(RemoveFurniture) { IsExecutable = true };
        }

        public ObservableCollection<Furniture> Furnitures { get; set; }

        private Furniture selectedItem;
        public Furniture SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public Room Room { get; set; }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        private void AddFurniture()
        {
            if (SelectedItem != null && Room != null)
            {
                dataContext.Furnitures.Add(SelectedItem);
                base.SaveChanges();
                dataContext.Rooms.Find(Room.RoomId).FurnitureId = SelectedItem.FurnitureId;
                IsAdding = false;
            }
        }

        private void RemoveFurniture()
        {
            if (SelectedItem != null)
            {
                dataContext.Furnitures.Remove(SelectedItem);
                base.SaveChanges();
                Furnitures.Clear();

                foreach (var furniture in dataContext.Furnitures)
                {
                    Furnitures.Add(furniture);
                }
            }
        }

        public void GetFurnitureByRoom(Room room)
        {
            Furnitures.Clear();

            if (room.Furniture != null)
                Furnitures.Add(room.Furniture);
        }
    }
}
