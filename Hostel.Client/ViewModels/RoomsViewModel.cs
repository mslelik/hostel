﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hostel.Data;
using System.Collections.ObjectModel;

namespace Hostel.Client.ViewModels
{
    public class RoomsViewModel : ViewModelBase
    {
        public RoomsViewModel()
        {
            dataContext = new HostelDBEntities();
            Rooms = new ObservableCollection<Room>(dataContext.Rooms);

            Conditions = new ObservableCollection<string>() { "Нежилая", "Плохое", "Среднее", "Хорошее", "Отличное" };

            Add = new ActionCommand(AddRoom) { IsExecutable = true };
            Remove = new ActionCommand(RemoveRoom) { IsExecutable = true };
            ViewAllRooms = new ActionCommand(AllRooms) { IsExecutable = true };
            Update = new ActionCommand(UpdateRooms) { IsExecutable = true };
        }

        public ObservableCollection<Room> Rooms { get; private set; }

        public ObservableCollection<string> Conditions { get; private set; }

        private Room selectedItem;
        public Room SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (value == null) IsSelected = false;
                else IsSelected = true;

                if (selectedItem == value) return;
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }

        public ActionCommand Add { get; set; }

        public ActionCommand Remove { get; set; }

        public ActionCommand ViewAllRooms { get; set; }

        public ActionCommand Update { get; set; }

        private void AddRoom()
        {
            if (SelectedItem != null)
            {
                dataContext.Rooms.Add(SelectedItem);
                IsAdding = false;
            }
        }

        private void RemoveRoom()
        {
            if (SelectedItem != null)
            {
                dataContext.Rooms.Remove(SelectedItem);
                base.SaveChanges();
                Rooms.Clear();

                foreach (var room in dataContext.Rooms)
                {
                    Rooms.Add(room);
                }
            }
        }

        private void AllRooms()
        {
            Rooms.Clear();
            foreach (var room in dataContext.Rooms)
            {
                Rooms.Add(room);
            }
        }

        private void UpdateRooms()
        {
            dataContext = new HostelDBEntities();
            AllRooms();
        }

        public void GetRoomByStudent(Student student)
        {
            Rooms.Clear();
            if (student.RoomId != null)
                Rooms.Add(student.Room);
        }
    }
}
