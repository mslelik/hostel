﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hostel.Client.ViewModels
{
    public class ActionCommand : ICommand
    {
        public ActionCommand(Action _action)
        {
            this.action = _action;
        }

        private Action action;
        private bool isExecutable;

        public bool IsExecutable
        {
            get { return isExecutable; }
            set
            {
                isExecutable = value;
                if (CanExecuteChanged == null) return;
                CanExecuteChanged(this, new EventArgs());
            }
        }

        public bool CanExecute(object parameter)
        {
            return IsExecutable;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            action();
        }
    }
}
